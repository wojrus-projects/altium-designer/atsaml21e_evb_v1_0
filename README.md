# Specyfikacja

Urządzenie jest uniwersalną płytką uruchomieniową do MCU ATSAML21E17B-AN (https://www.microchip.com/en-us/product/ATSAML21E17B).

# Projekt PCB

Schemat: [doc/ATSAML21E_EVB_V1_0_SCH.pdf](doc/ATSAML21E_EVB_V1_0_SCH.pdf)

Widok 3D: [doc/ATSAML21E_EVB_V1_0_3D.pdf](doc/ATSAML21E_EVB_V1_0_3D.pdf) (wymaga *Adobe Acrobat Reader DC*)

# Licencja

MIT
